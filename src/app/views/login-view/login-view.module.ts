import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginViewComponent } from './login-view.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: LoginViewComponent
  }
];

@NgModule({
  declarations: [ LoginViewComponent ],
  // To be able to use common module features on the pokeblock component
  // you need to import the common module in the component that uses it
  imports:[ CommonModule, RouterModule.forChild(routes) ],
  exports:[ RouterModule ]
})
export class LoginViewModule {}