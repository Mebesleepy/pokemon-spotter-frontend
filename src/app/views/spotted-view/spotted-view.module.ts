import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SpottedViewComponent } from './spotted-view.component';
import { PokeblockComponent } from '../../components/pokeblock/pokeblock.component'
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: SpottedViewComponent
  }
];

@NgModule({
  declarations: [ SpottedViewComponent, PokeblockComponent ],
  // To be able to use common module features on the pokeblock component
  // you need to import the common module in the component that uses it
  imports:[ RouterModule.forChild(routes), CommonModule ],
  exports:[ RouterModule ]
})
export class SpottedViewModule {}