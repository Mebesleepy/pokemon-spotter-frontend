import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpotterViewComponent } from './spotter-view.component';

describe('SpotterViewComponent', () => {
  let component: SpotterViewComponent;
  let fixture: ComponentFixture<SpotterViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpotterViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpotterViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
