import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as L from 'leaflet'
import { PokeapiService } from 'src/app/services/pokeapi/pokeapi.service';
import { BackendService } from 'src/app/services/backend/backend.service';

@Component({
  selector: 'app-spotter-view',
  templateUrl: './spotter-view.component.html',
  styleUrls: ['./spotter-view.component.scss']
})
export class SpotterViewComponent implements AfterViewInit {
  // Leaflet variables
  private map
  private latitude: Number = 56.877796
  private longitude: Number = 14.807017
  private zoom: Number = 17
  private marker = L.marker()
  private mapMarkerIcon = null

  private spottedPokemon: any

  constructor(private backendService: BackendService, private pokeApiService: PokeapiService) { }

  ngAfterViewInit(): void {
    this.initMap()
  }

  private initMap(): void {
    this.map = L.map('map').setView([this.latitude, this.longitude], this.zoom);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);

    // Set eventlistener on the map
    this.map.on('click', this.onMapClick.bind(this))
  }
  

  async onMapClick(event): Promise<any> {
    this.latitude = event.latlng.lat
    this.longitude = event.latlng.lng

    const fetchedPokemon = await this.pokeApiService.getRandomPokemon()

    this.setSpottedPokemon(fetchedPokemon)
    
    this.setMarkerIcon(fetchedPokemon.sprites, this.spottedPokemon.shiny)

    this.setMarkerPosition(this.latitude, this.longitude)

    this.setMarkerPopup(this.spottedPokemon, fetchedPokemon.name)

    await this.savePokemonToDatabase(this.spottedPokemon)
  }
  
  private setMarkerIcon(sprites: any, shiny: boolean): void {
    if (shiny) {
      this.mapMarkerIcon = L.icon({
        iconUrl: sprites.front_shiny,
        iconAnchor: [50, 40]
      })
    }
    else {
      this.mapMarkerIcon = L.icon({
        iconUrl: sprites.front_default,
        iconAnchor: [50, 40]
      })
    }
  }

  private setMarkerPosition(latitude: Number, longitude: Number): void {
    this.map.removeLayer(this.marker)
    this.marker = L.marker([latitude, longitude], { icon: this.mapMarkerIcon }).addTo(this.map)
  }

  private setMarkerPopup(spottedPokemon, name: string): void {
    this.marker.bindPopup(`<b>You spotted ${this.capitalizeFirstLetter(name)}!</b>
                          <br>Gender: ${spottedPokemon.gender === 'm' ? 'Male' : 'Female'}
                          <br>Shiny: ${spottedPokemon.shiny === true ? 'Yes!' : 'No'}`).openPopup()
  }

  // Generate random number between 0 - 1. 0 for male and 1 for female
  private getRandomGender(): string {
    const random = Math.round(Math.random() * (1 - 0))
    return random === 0 ? 'm' : 'f'
  }

  // Generate random number between 1 - 100. Above 90 returns true for Shiny
  private getShinyStatus(): boolean {
    return Math.round(Math.random() * (100 - 1) + 1) > 90
  }

  private setSpottedPokemon(fetchedPokemon): void {
    this.spottedPokemon = {
      caughtBy: window.localStorage.getItem('user'),
      id: fetchedPokemon.id + '',
      latitude: this.latitude + '',
      longitude: this.longitude + '',
      gender: this.getRandomGender(),
      shiny: this.getShinyStatus()
    }
  }

  private async savePokemonToDatabase(pokemon) {
    try {
      const response = await this.backendService.addSpottedPokemon(pokemon)
      // TODO: Do something with response message (e.g. display success/error message)
    } catch (error) {
      console.error(error);
    }
  }

  private capitalizeFirstLetter(string: string): string {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

}
