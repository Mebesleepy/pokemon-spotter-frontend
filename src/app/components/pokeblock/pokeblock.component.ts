import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pokeblock',
  templateUrl: './pokeblock.component.html',
  styleUrls: ['./pokeblock.component.scss']
})
export class PokeblockComponent implements OnInit {

  // Input parameter given in the "<app-pokeblock>" tag
  @Input() pokedata;

  constructor() { }

  ngOnInit(): void {
  }

}
