import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnonymousViewComponent } from './views/anonymous-view/anonymous-view.component'
import { PlayerStatsViewComponent } from './views/player-stats-view/player-stats-view.component'
import { AuthGuard } from './guards/auth.guard'
import { SpotterViewComponent } from './views/spotter-view/spotter-view.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: 'anonymous',
    component: AnonymousViewComponent
  },
  {
    path: 'login',
    loadChildren: ()=> import('./views/login-view/login-view.module').then(m => m.LoginViewModule)
  },
  {
    path: 'playerstat',
    component: PlayerStatsViewComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'spottedview',
    canActivate: [ AuthGuard ],
    // Load the page using lazy loading (Load only when page is being requested)
    loadChildren: ()=> import('./views/spotted-view/spotted-view.module').then(m => m.SpottedViewModule)
  },
  {
    path: 'spotter',
    component: SpotterViewComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/anonymous'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
