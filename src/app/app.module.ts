import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SpotterViewComponent } from './views/spotter-view/spotter-view.component';
import { AnonymousViewComponent } from './views/anonymous-view/anonymous-view.component';
import { PlayerStatsViewComponent } from './views/player-stats-view/player-stats-view.component'

@NgModule({
  declarations: [
    AppComponent,
    SpotterViewComponent,
    AnonymousViewComponent,
    PlayerStatsViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
