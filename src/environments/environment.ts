export const environment = {
  production: false,
  pokeapiUrl: 'https://pokeapi.co/api/v2',
  backendApiUrl: ' https://pokemon-spotter-backend.herokuapp.com'
};