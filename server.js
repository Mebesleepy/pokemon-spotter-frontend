var express = require('express'),
   path = require('path'),
   fs = require('fs');
var compression = require('compression');
var app = express();
var staticRoot = __dirname + '/dist/Angular/';
// var env = process.env.NODE_ENV || 'development';
app.set('port', (process.env.PORT || 8000));
app.use(compression());
app.use(function(req, res, next) {
    var accept = req.accepts('html', 'json', 'xml');
    if (accept !== 'html') {
        return next();
    }
    var ext = path.extname(req.path);
    if (ext !== '') {
        return next();
    }
    fs.createReadStream(staticRoot + 'index.html').pipe(res);
});
app.use(express.static(staticRoot));
app.listen(app.get('port'), function() {
    console.log('app running on port', app.get('port'));
});